$(document).ready(function() {
    'use strict'

    if($(document).scrollTop() > 10) {
        $('.c-headpanel').addClass('u-mrg-t--45__neg');
    }

    $(window).scroll(function() {
        if ($(document).scrollTop() > 10) {
            $('.c-headpanel').addClass('u-mrg-t--45__neg');
        } else {
            $('.c-headpanel').removeClass('u-mrg-t--45__neg');
        }
    });
});