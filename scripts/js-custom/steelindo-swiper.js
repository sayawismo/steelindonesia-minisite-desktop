// swiper contact home
var swiper = new Swiper('.swiper-contact-home', {
    slidesPerView: 4,
    spaceBetween: 25,
    freeMode: true,
    loop: false,
    navigation: {
        nextEl: '.c-btn--next__contact-mkt',
        prevEl: '.c-btn--prev__contact-mkt',
        clickable: true,
    },
});

// swiper other product
var swiper = new Swiper('.swiper-other-product', {
    slidesPerView: 6,
    spaceBetween: 15,
    freeMode: false,
    loop: false,
    navigation: {
        nextEl: '.c-btn--next__other-product',
        prevEl: '.c-btn--prev__other-product',
        clickable: true,
    },
});

// swiper product detail image
var swiper = new Swiper('.swiper-prd-detail-image', {
    pagination: {
      el: '.swiper-pagination',
    },
});

var swiper = new Swiper('.swiper-so-detail-image', {
    pagination: {
      el: '.swiper-pagination',
    },
});

// swiper contact page
var swiper = new Swiper('.swiper-contact-page', {
    slidesPerView: 4,
    spaceBetween: 15,
    freeMode: true,
    loop: false,
    navigation: {
        nextEl: '.c-btn--next__contact-page',
        prevEl: '.c-btn--prev__contact-page',
        clickable: true,
    },
});

// swiper home banner
var swiper = new Swiper('.swiper-banner-home', {
    loop: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
});

// superfish navbar
$(function() {
    'use strict'
    $('#navbarMain').superfish();

    // show hide menu, for mobile only
    $('#showMenu').on('click', function(){
        if($('#navbarMain').hasClass('hidden-md-down')) {
            $('#navbarMain').removeClass('hidden-md-down');
            $('body').addClass('show-sf-menu');

            // change navicon icon to close icon button
            $(this).find('.fa').removeClass('fa-navicon').addClass('fa-close');

        } else {
            $('#navbarMain').addClass('hidden-md-down');
            $('body').removeClass('show-sf-menu');

            // change back from close to navicon icon button
            $(this).find('.fa').removeClass('fa-close').addClass('fa-navicon');
        }
        return false;
    });
});